<?php


namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CalculatorType extends AbstractType {

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('firstNumber', null, ['attr' => ['class'=>'form-control', 'placeholder'=>'First Number'], 'label' => false])
			->add('operand', ChoiceType::class, [
				'choices' => [
					'+' => 'add',
					'-' => 'subtract',
					'*' => 'multiply',
					'/' => 'divide'
				],
				'attr' => ['class'=>'form-control'],
				'label' => false
			])
			->add('secondNumber', null, ['attr' => ['class'=>'form-control', 'placeholder'=>'Second Number'], 'label' => false])
			->add('Calculate', SubmitType::class,  ['attr' => ['class'=>'btn btn-primary']]);
	}

}