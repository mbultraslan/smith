<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Calculator;
use App\Form\CalculatorType;
use Symfony\Component\HttpFoundation\Request;

class CalculatorController extends AbstractController {


	/**
	 * @Route("/")
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function index(Request $request){

		$calculator = new Calculator();
		$form = $this->createForm(CalculatorType::class, $calculator, ['attr'=>['class'=>'form-inline']]);
		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			$calculator = $form->getData();
			$result = $calculator->calculate();
			return $this->render('calculator/calculator.html.twig', array(
					'form' => $form->createView(),
					'result' => $result,
					'data'=>$calculator
				)
			);
		}
		return $this->render('calculator/calculator.html.twig', array('form' => $form->createView()));
	}
}