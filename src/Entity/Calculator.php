<?php


namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;


class Calculator
{
	/**
	 * @Assert\Type("integer")
	 */
	public $firstNumber;
	/**
	 * @Assert\Type("integer")
	 */
	public $secondNumber;
	/**
	 * @Assert\Choice({"add", "subtract", "multiply", "divide"})
	 */
	public $operand;
	/**
	 * @return integer
	 */
	public function getFirstNumber()
	{
		return $this->firstNumber;
	}
	/**
	 * @param integer $firstNumber
	 */
	public function setFirstNumber($firstNumber)
	{
		$this->firstNumber = $firstNumber;
	}
	/**
	 * @return integer
	 */
	public function getSecondNumber()
	{
		return $this->secondNumber;
	}
	/**
	 * @param integer $secondNumber
	 */
	public function setSecondNumber($secondNumber)
	{
		$this->secondNumber = $secondNumber;
	}
	/**
	 * @return integer
	 */
	public function getOperand()
	{
		return $this->operand;
	}
	/**
	 * @param integer $operand
	 */
	public function setOperand($operand)
	{
		$this->operand = $operand;
	}
	public function calculate()
	{
		try{
			return $this->{$this->getOperand()}($this->getFirstNumber(), $this->getSecondNumber());

		}
		catch (\Exception $e){
			die('Operand not valid');
		}

	}


	private function add($firstNumber, $secondNumber)
	{
		return $firstNumber + $secondNumber;
	}

	private function subtract($firstNumber, $secondNumber)
	{
		return $firstNumber - $secondNumber;
	}

	private function multiply($firstNumber, $secondNumber)
	{
		return $firstNumber * $secondNumber;
	}

	private function divide($firstNumber, $secondNumber)
	{
		return $firstNumber / $secondNumber;
	}
}